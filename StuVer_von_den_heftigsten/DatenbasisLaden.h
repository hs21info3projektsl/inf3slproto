#pragma once
#include <direct.h>
#include <stdlib.h>

namespace StuVer_von_den_heftigsten {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r DatenbasisLaden
	/// </summary>
	public ref class DatenbasisLaden : public System::Windows::Forms::Form
	{
	public:
		DatenbasisLaden(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
			String^ str ="";
			char* buffer;

			// Den momentanen Dateipfad des Programmes ermitteln 
			if( (buffer = _getcwd( NULL, 0 )) != NULL )
			{
				str = System::Convert::ToString(buffer);
			}
			// Schreibe den Dateipfad als Initialisierung in openFileDialog
			openFileDialog1->InitialDirectory = str;
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~DatenbasisLaden()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::Button^  read_file;
	//protected: 
	public: System::Windows::Forms::TextBox^  textBox1;
	public: System::Windows::Forms::Label^  label1;
	public: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	public: System::Windows::Forms::Button^  �bernehmen;
	private: 


	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->read_file = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->�bernehmen = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// read_file
			// 
			this->read_file->Location = System::Drawing::Point(12, 12);
			this->read_file->Name = L"read_file";
			this->read_file->Size = System::Drawing::Size(125, 23);
			this->read_file->TabIndex = 1;
			this->read_file->Text = L"W�hle Datei";
			this->read_file->UseVisualStyleBackColor = true;
			this->read_file->Click += gcnew System::EventHandler(this, &DatenbasisLaden::read_file_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 72);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(464, 22);
			this->textBox1->TabIndex = 2;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 52);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(73, 17);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Dateipfad:";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// �bernehmen
			// 
			this->�bernehmen->Location = System::Drawing::Point(15, 113);
			this->�bernehmen->Name = L"�bernehmen";
			this->�bernehmen->Size = System::Drawing::Size(109, 24);
			this->�bernehmen->TabIndex = 4;
			this->�bernehmen->Text = L"�bernehmen";
			this->�bernehmen->UseVisualStyleBackColor = true;
			this->�bernehmen->Click += gcnew System::EventHandler(this, &DatenbasisLaden::�bernehmen_Click);
			// 
			// DatenbasisLaden
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(518, 149);
			this->Controls->Add(this->�bernehmen);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->read_file);
			this->Name = L"DatenbasisLaden";
			this->Text = L"DatenbasisLaden";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
public: System::Void read_file_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void �bernehmen_Click(System::Object^  sender, System::EventArgs^  e);
};
}
