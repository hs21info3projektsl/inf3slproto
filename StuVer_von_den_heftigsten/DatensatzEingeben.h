#pragma once

namespace StuVer_von_den_heftigsten {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;



	/// <summary>
	/// Zusammenfassung f�r DatensatzEingeben
	/// </summary>
	public ref class DatensatzEingeben : public System::Windows::Forms::Form
	{
	public:
		DatensatzEingeben(int i)	//Je nachdem von wo das Fenster konstruiert wird, werden zus�tzliche Buttons verwendet
		{


			InitializeComponent();
			if(i == 0)
			{	
				this->butDiffSave->Enabled = false;
				this->butDiffSave->Visible = false;
				this->butDelete->Enabled = false;
				this->butDelete->Visible = false;
				this->butSave->Enabled = true;
				this->butSave->Visible = true;
			}
			else if ( i == 1)
			{
				this->butDiffSave->Enabled = true;
				this->butDiffSave->Visible = true;
				this->butDelete->Enabled = true;
				this->butDelete->Visible = true;
				this->butSave->Enabled = false;
				this->butSave->Visible = false;
			}
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
		


		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~DatensatzEingeben()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  studMatr;
	protected: 
	private: System::Windows::Forms::Label^  studName;
	private: System::Windows::Forms::Label^  studVorName;
	private: System::Windows::Forms::Label^  studDate;
	private: System::Windows::Forms::Label^  studPhoneHome;
	private: System::Windows::Forms::Label^  studPhoneMobile;
	private: System::Windows::Forms::Label^  studEmail;
	private: System::Windows::Forms::Label^  studStreet;
	private: System::Windows::Forms::Label^  studHausNr;
	private: System::Windows::Forms::Label^  studPLZ;
	private: System::Windows::Forms::Label^  studOrt;
	private: System::Windows::Forms::Label^  praxName;
	private: System::Windows::Forms::Label^  praxStreet;
	private: System::Windows::Forms::Label^  praxHausNr;
	private: System::Windows::Forms::Label^  praxPLZ;
	private: System::Windows::Forms::Label^  praxOrt;
	private: System::Windows::Forms::Label^  betrName;
	private: System::Windows::Forms::Label^  betrVorName;
	private: System::Windows::Forms::Label^  betrPos;
	private: System::Windows::Forms::Label^  betrPhoneHome;
	private: System::Windows::Forms::Label^  betrPhoneMobile;
	private: System::Windows::Forms::Label^  betrEmail;
	public: System::Windows::Forms::GroupBox^  groupStud;
	public: System::Windows::Forms::GroupBox^  groupBox1;
	public: System::Windows::Forms::GroupBox^  groupBox2;
	public: System::Windows::Forms::TextBox^  tboxStudOrt;
	public: System::Windows::Forms::TextBox^  tboxStudPLZ;
	public: System::Windows::Forms::TextBox^  tboxStudHausNr;
	public: System::Windows::Forms::TextBox^  tboxStudStra�e;
	public: System::Windows::Forms::TextBox^  tboxStudEmail;
	public: System::Windows::Forms::TextBox^  tboxStudPhoneMobile;
	public: System::Windows::Forms::TextBox^  tboxStudPhoneHome;
	public: System::Windows::Forms::TextBox^  tboxStudDatum;
	public: System::Windows::Forms::TextBox^  tboxStudVorName;
	public: System::Windows::Forms::TextBox^  tBoxStudName;
	public: System::Windows::Forms::TextBox^  tboxStudMatr;
	public: System::Windows::Forms::TextBox^  tboxPraxOrt;
	public: System::Windows::Forms::TextBox^  tboxPraxPLZ;
	public: System::Windows::Forms::TextBox^  tboxPraxHausNr;
	public: System::Windows::Forms::TextBox^  tboxPraxStreet;
	public: System::Windows::Forms::TextBox^  tboxPraxName;
	public: System::Windows::Forms::TextBox^  tboxBetrEmail;
	public: System::Windows::Forms::TextBox^  tboxBetrPhoneMobile;
	public: System::Windows::Forms::TextBox^  tboxBetrPhoneHome;
	public: System::Windows::Forms::TextBox^  tboxBetrPos;
	public: System::Windows::Forms::TextBox^  tboxBetrVorName;
	public: System::Windows::Forms::TextBox^  tboxBetrName;

	private: System::Windows::Forms::Label^  label1;
	public: System::Windows::Forms::TextBox^  MessageBox;
	private: 

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  butDiffSave;
	private: System::Windows::Forms::Button^  butSave;
	private: System::Windows::Forms::Button^  butDelete;





	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->studMatr = (gcnew System::Windows::Forms::Label());
			this->studName = (gcnew System::Windows::Forms::Label());
			this->studVorName = (gcnew System::Windows::Forms::Label());
			this->studDate = (gcnew System::Windows::Forms::Label());
			this->studPhoneHome = (gcnew System::Windows::Forms::Label());
			this->studPhoneMobile = (gcnew System::Windows::Forms::Label());
			this->studEmail = (gcnew System::Windows::Forms::Label());
			this->studStreet = (gcnew System::Windows::Forms::Label());
			this->studHausNr = (gcnew System::Windows::Forms::Label());
			this->studPLZ = (gcnew System::Windows::Forms::Label());
			this->studOrt = (gcnew System::Windows::Forms::Label());
			this->praxName = (gcnew System::Windows::Forms::Label());
			this->praxStreet = (gcnew System::Windows::Forms::Label());
			this->praxHausNr = (gcnew System::Windows::Forms::Label());
			this->praxPLZ = (gcnew System::Windows::Forms::Label());
			this->praxOrt = (gcnew System::Windows::Forms::Label());
			this->betrName = (gcnew System::Windows::Forms::Label());
			this->betrVorName = (gcnew System::Windows::Forms::Label());
			this->betrPos = (gcnew System::Windows::Forms::Label());
			this->betrPhoneHome = (gcnew System::Windows::Forms::Label());
			this->betrPhoneMobile = (gcnew System::Windows::Forms::Label());
			this->betrEmail = (gcnew System::Windows::Forms::Label());
			this->groupStud = (gcnew System::Windows::Forms::GroupBox());
			this->tboxStudOrt = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudPLZ = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudHausNr = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudStra�e = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudEmail = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudPhoneMobile = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudPhoneHome = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudDatum = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudVorName = (gcnew System::Windows::Forms::TextBox());
			this->tBoxStudName = (gcnew System::Windows::Forms::TextBox());
			this->tboxStudMatr = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->tboxPraxOrt = (gcnew System::Windows::Forms::TextBox());
			this->tboxPraxPLZ = (gcnew System::Windows::Forms::TextBox());
			this->tboxPraxHausNr = (gcnew System::Windows::Forms::TextBox());
			this->tboxPraxStreet = (gcnew System::Windows::Forms::TextBox());
			this->tboxPraxName = (gcnew System::Windows::Forms::TextBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->tboxBetrEmail = (gcnew System::Windows::Forms::TextBox());
			this->tboxBetrPhoneMobile = (gcnew System::Windows::Forms::TextBox());
			this->tboxBetrPhoneHome = (gcnew System::Windows::Forms::TextBox());
			this->tboxBetrPos = (gcnew System::Windows::Forms::TextBox());
			this->tboxBetrVorName = (gcnew System::Windows::Forms::TextBox());
			this->tboxBetrName = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->MessageBox = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->butDiffSave = (gcnew System::Windows::Forms::Button());
			this->butSave = (gcnew System::Windows::Forms::Button());
			this->butDelete = (gcnew System::Windows::Forms::Button());
			this->groupStud->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// studMatr
			// 
			this->studMatr->AutoSize = true;
			this->studMatr->Location = System::Drawing::Point(25, 20);
			this->studMatr->Name = L"studMatr";
			this->studMatr->Size = System::Drawing::Size(166, 17);
			this->studMatr->TabIndex = 0;
			this->studMatr->Text = L"Student Matrikelnummer*";
			// 
			// studName
			// 
			this->studName->AutoSize = true;
			this->studName->Location = System::Drawing::Point(25, 45);
			this->studName->Name = L"studName";
			this->studName->Size = System::Drawing::Size(103, 17);
			this->studName->TabIndex = 1;
			this->studName->Text = L"Student Name*";
			// 
			// studVorName
			// 
			this->studVorName->AutoSize = true;
			this->studVorName->Location = System::Drawing::Point(25, 70);
			this->studVorName->Name = L"studVorName";
			this->studVorName->Size = System::Drawing::Size(123, 17);
			this->studVorName->TabIndex = 2;
			this->studVorName->Text = L"Student Vorname*";
			// 
			// studDate
			// 
			this->studDate->AutoSize = true;
			this->studDate->Location = System::Drawing::Point(25, 95);
			this->studDate->Name = L"studDate";
			this->studDate->Size = System::Drawing::Size(130, 17);
			this->studDate->TabIndex = 3;
			this->studDate->Text = L"Student Startdatum";
			// 
			// studPhoneHome
			// 
			this->studPhoneHome->AutoSize = true;
			this->studPhoneHome->Location = System::Drawing::Point(25, 120);
			this->studPhoneHome->Name = L"studPhoneHome";
			this->studPhoneHome->Size = System::Drawing::Size(188, 17);
			this->studPhoneHome->TabIndex = 4;
			this->studPhoneHome->Text = L"Student Haustelefonnummer";
			// 
			// studPhoneMobile
			// 
			this->studPhoneMobile->AutoSize = true;
			this->studPhoneMobile->Location = System::Drawing::Point(25, 145);
			this->studPhoneMobile->Name = L"studPhoneMobile";
			this->studPhoneMobile->Size = System::Drawing::Size(188, 17);
			this->studPhoneMobile->TabIndex = 5;
			this->studPhoneMobile->Text = L"Student Mobiltelefonnummer";
			// 
			// studEmail
			// 
			this->studEmail->AutoSize = true;
			this->studEmail->Location = System::Drawing::Point(25, 170);
			this->studEmail->Name = L"studEmail";
			this->studEmail->Size = System::Drawing::Size(95, 17);
			this->studEmail->TabIndex = 6;
			this->studEmail->Text = L"Student Email";
			// 
			// studStreet
			// 
			this->studStreet->AutoSize = true;
			this->studStreet->Location = System::Drawing::Point(25, 195);
			this->studStreet->Name = L"studStreet";
			this->studStreet->Size = System::Drawing::Size(104, 17);
			this->studStreet->TabIndex = 7;
			this->studStreet->Text = L"Student Stra�e";
			// 
			// studHausNr
			// 
			this->studHausNr->AutoSize = true;
			this->studHausNr->Location = System::Drawing::Point(25, 220);
			this->studHausNr->Name = L"studHausNr";
			this->studHausNr->Size = System::Drawing::Size(145, 17);
			this->studHausNr->TabIndex = 8;
			this->studHausNr->Text = L"Student Hausnummer";
			// 
			// studPLZ
			// 
			this->studPLZ->AutoSize = true;
			this->studPLZ->Location = System::Drawing::Point(25, 245);
			this->studPLZ->Name = L"studPLZ";
			this->studPLZ->Size = System::Drawing::Size(87, 17);
			this->studPLZ->TabIndex = 9;
			this->studPLZ->Text = L"Student PLZ";
			// 
			// studOrt
			// 
			this->studOrt->AutoSize = true;
			this->studOrt->Location = System::Drawing::Point(25, 270);
			this->studOrt->Name = L"studOrt";
			this->studOrt->Size = System::Drawing::Size(115, 17);
			this->studOrt->TabIndex = 10;
			this->studOrt->Text = L"Student Wohnort";
			// 
			// praxName
			// 
			this->praxName->AutoSize = true;
			this->praxName->Location = System::Drawing::Point(20, 20);
			this->praxName->Name = L"praxName";
			this->praxName->Size = System::Drawing::Size(133, 17);
			this->praxName->TabIndex = 11;
			this->praxName->Text = L"Praxispartner Name";
			// 
			// praxStreet
			// 
			this->praxStreet->AutoSize = true;
			this->praxStreet->Location = System::Drawing::Point(20, 45);
			this->praxStreet->Name = L"praxStreet";
			this->praxStreet->Size = System::Drawing::Size(139, 17);
			this->praxStreet->TabIndex = 12;
			this->praxStreet->Text = L"Praxispartner Stra�e";
			// 
			// praxHausNr
			// 
			this->praxHausNr->AutoSize = true;
			this->praxHausNr->Location = System::Drawing::Point(20, 70);
			this->praxHausNr->Name = L"praxHausNr";
			this->praxHausNr->Size = System::Drawing::Size(180, 17);
			this->praxHausNr->TabIndex = 13;
			this->praxHausNr->Text = L"Praxispartner Hausnummer";
			// 
			// praxPLZ
			// 
			this->praxPLZ->AutoSize = true;
			this->praxPLZ->Location = System::Drawing::Point(20, 95);
			this->praxPLZ->Name = L"praxPLZ";
			this->praxPLZ->Size = System::Drawing::Size(122, 17);
			this->praxPLZ->TabIndex = 14;
			this->praxPLZ->Text = L"Praxispartner PLZ";
			// 
			// praxOrt
			// 
			this->praxOrt->AutoSize = true;
			this->praxOrt->Location = System::Drawing::Point(20, 120);
			this->praxOrt->Name = L"praxOrt";
			this->praxOrt->Size = System::Drawing::Size(116, 17);
			this->praxOrt->TabIndex = 15;
			this->praxOrt->Text = L"Praxispartner Ort";
			// 
			// betrName
			// 
			this->betrName->AutoSize = true;
			this->betrName->Location = System::Drawing::Point(20, 20);
			this->betrName->Name = L"betrName";
			this->betrName->Size = System::Drawing::Size(104, 17);
			this->betrName->TabIndex = 16;
			this->betrName->Text = L"Betreuer Name";
			// 
			// betrVorName
			// 
			this->betrVorName->AutoSize = true;
			this->betrVorName->Location = System::Drawing::Point(20, 45);
			this->betrVorName->Name = L"betrVorName";
			this->betrVorName->Size = System::Drawing::Size(124, 17);
			this->betrVorName->TabIndex = 17;
			this->betrVorName->Text = L"Betreuer Vorname";
			// 
			// betrPos
			// 
			this->betrPos->AutoSize = true;
			this->betrPos->Location = System::Drawing::Point(20, 70);
			this->betrPos->Name = L"betrPos";
			this->betrPos->Size = System::Drawing::Size(117, 17);
			this->betrPos->TabIndex = 18;
			this->betrPos->Text = L"Betreuer Position";
			// 
			// betrPhoneHome
			// 
			this->betrPhoneHome->AutoSize = true;
			this->betrPhoneHome->Location = System::Drawing::Point(20, 95);
			this->betrPhoneHome->Name = L"betrPhoneHome";
			this->betrPhoneHome->Size = System::Drawing::Size(194, 17);
			this->betrPhoneHome->TabIndex = 19;
			this->betrPhoneHome->Text = L"Betreuer Haustelefonnummer";
			// 
			// betrPhoneMobile
			// 
			this->betrPhoneMobile->AutoSize = true;
			this->betrPhoneMobile->Location = System::Drawing::Point(20, 120);
			this->betrPhoneMobile->Name = L"betrPhoneMobile";
			this->betrPhoneMobile->Size = System::Drawing::Size(151, 17);
			this->betrPhoneMobile->TabIndex = 20;
			this->betrPhoneMobile->Text = L"Betreuer Mobilnummer";
			// 
			// betrEmail
			// 
			this->betrEmail->AutoSize = true;
			this->betrEmail->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->betrEmail->Location = System::Drawing::Point(20, 145);
			this->betrEmail->Name = L"betrEmail";
			this->betrEmail->Size = System::Drawing::Size(101, 17);
			this->betrEmail->TabIndex = 21;
			this->betrEmail->Text = L"Betreuer Email";
			// 
			// groupStud
			// 
			this->groupStud->Controls->Add(this->tboxStudOrt);
			this->groupStud->Controls->Add(this->tboxStudPLZ);
			this->groupStud->Controls->Add(this->tboxStudHausNr);
			this->groupStud->Controls->Add(this->tboxStudStra�e);
			this->groupStud->Controls->Add(this->tboxStudEmail);
			this->groupStud->Controls->Add(this->tboxStudPhoneMobile);
			this->groupStud->Controls->Add(this->tboxStudPhoneHome);
			this->groupStud->Controls->Add(this->tboxStudDatum);
			this->groupStud->Controls->Add(this->tboxStudVorName);
			this->groupStud->Controls->Add(this->tBoxStudName);
			this->groupStud->Controls->Add(this->tboxStudMatr);
			this->groupStud->Controls->Add(this->studOrt);
			this->groupStud->Controls->Add(this->studPLZ);
			this->groupStud->Controls->Add(this->studHausNr);
			this->groupStud->Controls->Add(this->studStreet);
			this->groupStud->Controls->Add(this->studEmail);
			this->groupStud->Controls->Add(this->studPhoneMobile);
			this->groupStud->Controls->Add(this->studPhoneHome);
			this->groupStud->Controls->Add(this->studDate);
			this->groupStud->Controls->Add(this->studVorName);
			this->groupStud->Controls->Add(this->studName);
			this->groupStud->Controls->Add(this->studMatr);
			this->groupStud->Location = System::Drawing::Point(10, 10);
			this->groupStud->Name = L"groupStud";
			this->groupStud->Size = System::Drawing::Size(470, 300);
			this->groupStud->TabIndex = 22;
			this->groupStud->TabStop = false;
			this->groupStud->Text = L"Student";
			// 
			// tboxStudOrt
			// 
			this->tboxStudOrt->Location = System::Drawing::Point(235, 267);
			this->tboxStudOrt->Name = L"tboxStudOrt";
			this->tboxStudOrt->Size = System::Drawing::Size(220, 22);
			this->tboxStudOrt->TabIndex = 21;
			this->tboxStudOrt->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudOrt_KeyPress);
			// 
			// tboxStudPLZ
			// 
			this->tboxStudPLZ->Location = System::Drawing::Point(235, 242);
			this->tboxStudPLZ->Name = L"tboxStudPLZ";
			this->tboxStudPLZ->Size = System::Drawing::Size(220, 22);
			this->tboxStudPLZ->TabIndex = 20;
			this->tboxStudPLZ->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudPLZ_KeyPress);
			// 
			// tboxStudHausNr
			// 
			this->tboxStudHausNr->Location = System::Drawing::Point(235, 217);
			this->tboxStudHausNr->Name = L"tboxStudHausNr";
			this->tboxStudHausNr->Size = System::Drawing::Size(220, 22);
			this->tboxStudHausNr->TabIndex = 19;
			this->tboxStudHausNr->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudHausNr_KeyPress);
			// 
			// tboxStudStra�e
			// 
			this->tboxStudStra�e->Location = System::Drawing::Point(235, 192);
			this->tboxStudStra�e->Name = L"tboxStudStra�e";
			this->tboxStudStra�e->Size = System::Drawing::Size(220, 22);
			this->tboxStudStra�e->TabIndex = 18;
			this->tboxStudStra�e->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudStra�e_KeyPress);
			// 
			// tboxStudEmail
			// 
			this->tboxStudEmail->Location = System::Drawing::Point(235, 167);
			this->tboxStudEmail->Name = L"tboxStudEmail";
			this->tboxStudEmail->Size = System::Drawing::Size(220, 22);
			this->tboxStudEmail->TabIndex = 17;
			this->tboxStudEmail->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudEmail_KeyPress);
			// 
			// tboxStudPhoneMobile
			// 
			this->tboxStudPhoneMobile->Location = System::Drawing::Point(235, 142);
			this->tboxStudPhoneMobile->Name = L"tboxStudPhoneMobile";
			this->tboxStudPhoneMobile->Size = System::Drawing::Size(220, 22);
			this->tboxStudPhoneMobile->TabIndex = 16;
			this->tboxStudPhoneMobile->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudPhoneMobile_KeyPress);
			// 
			// tboxStudPhoneHome
			// 
			this->tboxStudPhoneHome->Location = System::Drawing::Point(235, 117);
			this->tboxStudPhoneHome->Name = L"tboxStudPhoneHome";
			this->tboxStudPhoneHome->Size = System::Drawing::Size(220, 22);
			this->tboxStudPhoneHome->TabIndex = 15;
			this->tboxStudPhoneHome->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudPhoneHome_KeyPress);
			// 
			// tboxStudDatum
			// 
			this->tboxStudDatum->Location = System::Drawing::Point(235, 92);
			this->tboxStudDatum->Name = L"tboxStudDatum";
			this->tboxStudDatum->Size = System::Drawing::Size(220, 22);
			this->tboxStudDatum->TabIndex = 14;
			this->tboxStudDatum->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudDatum_KeyPress);
			// 
			// tboxStudVorName
			// 
			this->tboxStudVorName->Location = System::Drawing::Point(235, 67);
			this->tboxStudVorName->Name = L"tboxStudVorName";
			this->tboxStudVorName->Size = System::Drawing::Size(220, 22);
			this->tboxStudVorName->TabIndex = 13;
			this->tboxStudVorName->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudVorName_KeyPress);
			// 
			// tBoxStudName
			// 
			this->tBoxStudName->Location = System::Drawing::Point(235, 42);
			this->tBoxStudName->Name = L"tBoxStudName";
			this->tBoxStudName->Size = System::Drawing::Size(220, 22);
			this->tBoxStudName->TabIndex = 12;
			this->tBoxStudName->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tBoxStudName_KeyPress);
			// 
			// tboxStudMatr
			// 
			this->tboxStudMatr->Location = System::Drawing::Point(235, 17);
			this->tboxStudMatr->Name = L"tboxStudMatr";
			this->tboxStudMatr->Size = System::Drawing::Size(220, 22);
			this->tboxStudMatr->TabIndex = 11;
			this->tboxStudMatr->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxStudMatr_KeyPress);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->tboxPraxOrt);
			this->groupBox1->Controls->Add(this->tboxPraxPLZ);
			this->groupBox1->Controls->Add(this->tboxPraxHausNr);
			this->groupBox1->Controls->Add(this->tboxPraxStreet);
			this->groupBox1->Controls->Add(this->tboxPraxName);
			this->groupBox1->Controls->Add(this->praxOrt);
			this->groupBox1->Controls->Add(this->praxPLZ);
			this->groupBox1->Controls->Add(this->praxHausNr);
			this->groupBox1->Controls->Add(this->praxStreet);
			this->groupBox1->Controls->Add(this->praxName);
			this->groupBox1->Location = System::Drawing::Point(10, 320);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(470, 150);
			this->groupBox1->TabIndex = 23;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Praxispartner";
			// 
			// tboxPraxOrt
			// 
			this->tboxPraxOrt->Location = System::Drawing::Point(235, 117);
			this->tboxPraxOrt->Name = L"tboxPraxOrt";
			this->tboxPraxOrt->Size = System::Drawing::Size(220, 22);
			this->tboxPraxOrt->TabIndex = 26;
			this->tboxPraxOrt->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxPraxOrt_KeyPress);
			// 
			// tboxPraxPLZ
			// 
			this->tboxPraxPLZ->Location = System::Drawing::Point(235, 92);
			this->tboxPraxPLZ->Name = L"tboxPraxPLZ";
			this->tboxPraxPLZ->Size = System::Drawing::Size(220, 22);
			this->tboxPraxPLZ->TabIndex = 25;
			this->tboxPraxPLZ->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxPraxPLZ_KeyPress);
			// 
			// tboxPraxHausNr
			// 
			this->tboxPraxHausNr->Location = System::Drawing::Point(235, 67);
			this->tboxPraxHausNr->Name = L"tboxPraxHausNr";
			this->tboxPraxHausNr->Size = System::Drawing::Size(220, 22);
			this->tboxPraxHausNr->TabIndex = 24;
			this->tboxPraxHausNr->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxPraxHausNr_KeyPress);
			// 
			// tboxPraxStreet
			// 
			this->tboxPraxStreet->Location = System::Drawing::Point(235, 42);
			this->tboxPraxStreet->Name = L"tboxPraxStreet";
			this->tboxPraxStreet->Size = System::Drawing::Size(220, 22);
			this->tboxPraxStreet->TabIndex = 23;
			this->tboxPraxStreet->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxPraxStreet_KeyPress);
			// 
			// tboxPraxName
			// 
			this->tboxPraxName->Location = System::Drawing::Point(235, 17);
			this->tboxPraxName->Name = L"tboxPraxName";
			this->tboxPraxName->Size = System::Drawing::Size(220, 22);
			this->tboxPraxName->TabIndex = 22;
			this->tboxPraxName->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxPraxName_KeyPress);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->tboxBetrEmail);
			this->groupBox2->Controls->Add(this->tboxBetrPhoneMobile);
			this->groupBox2->Controls->Add(this->tboxBetrPhoneHome);
			this->groupBox2->Controls->Add(this->tboxBetrPos);
			this->groupBox2->Controls->Add(this->tboxBetrVorName);
			this->groupBox2->Controls->Add(this->tboxBetrName);
			this->groupBox2->Controls->Add(this->betrEmail);
			this->groupBox2->Controls->Add(this->betrPhoneMobile);
			this->groupBox2->Controls->Add(this->betrPhoneHome);
			this->groupBox2->Controls->Add(this->betrPos);
			this->groupBox2->Controls->Add(this->betrVorName);
			this->groupBox2->Controls->Add(this->betrName);
			this->groupBox2->Location = System::Drawing::Point(10, 484);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(470, 175);
			this->groupBox2->TabIndex = 24;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"groupBox2";
			// 
			// tboxBetrEmail
			// 
			this->tboxBetrEmail->Location = System::Drawing::Point(235, 142);
			this->tboxBetrEmail->Name = L"tboxBetrEmail";
			this->tboxBetrEmail->Size = System::Drawing::Size(220, 22);
			this->tboxBetrEmail->TabIndex = 32;
			this->tboxBetrEmail->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxBetrEmail_KeyPress);
			// 
			// tboxBetrPhoneMobile
			// 
			this->tboxBetrPhoneMobile->Location = System::Drawing::Point(235, 117);
			this->tboxBetrPhoneMobile->Name = L"tboxBetrPhoneMobile";
			this->tboxBetrPhoneMobile->Size = System::Drawing::Size(220, 22);
			this->tboxBetrPhoneMobile->TabIndex = 31;
			this->tboxBetrPhoneMobile->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxBetrPhoneMobile_KeyPress);
			// 
			// tboxBetrPhoneHome
			// 
			this->tboxBetrPhoneHome->Location = System::Drawing::Point(235, 92);
			this->tboxBetrPhoneHome->Name = L"tboxBetrPhoneHome";
			this->tboxBetrPhoneHome->Size = System::Drawing::Size(220, 22);
			this->tboxBetrPhoneHome->TabIndex = 30;
			this->tboxBetrPhoneHome->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxBetrPhoneHome_KeyPress);
			// 
			// tboxBetrPos
			// 
			this->tboxBetrPos->Location = System::Drawing::Point(235, 67);
			this->tboxBetrPos->Name = L"tboxBetrPos";
			this->tboxBetrPos->Size = System::Drawing::Size(220, 22);
			this->tboxBetrPos->TabIndex = 29;
			this->tboxBetrPos->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxBetrPos_KeyPress);
			// 
			// tboxBetrVorName
			// 
			this->tboxBetrVorName->Location = System::Drawing::Point(235, 42);
			this->tboxBetrVorName->Name = L"tboxBetrVorName";
			this->tboxBetrVorName->Size = System::Drawing::Size(220, 22);
			this->tboxBetrVorName->TabIndex = 28;
			this->tboxBetrVorName->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxBetrVorName_KeyPress);
			// 
			// tboxBetrName
			// 
			this->tboxBetrName->Location = System::Drawing::Point(235, 17);
			this->tboxBetrName->Name = L"tboxBetrName";
			this->tboxBetrName->Size = System::Drawing::Size(220, 22);
			this->tboxBetrName->TabIndex = 27;
			this->tboxBetrName->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &DatensatzEingeben::tboxBetrName_KeyPress);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->label1->Location = System::Drawing::Point(135, 673);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(90, 17);
			this->label1->TabIndex = 33;
			this->label1->Text = L"* = Pflichtfeld";
			// 
			// MessageBox
			// 
			this->MessageBox->Location = System::Drawing::Point(30, 722);
			this->MessageBox->Name = L"MessageBox";
			this->MessageBox->ReadOnly = true;
			this->MessageBox->Size = System::Drawing::Size(220, 22);
			this->MessageBox->TabIndex = 33;
			this->MessageBox->Text = L"Alles in Ordnung.";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->label2->Location = System::Drawing::Point(30, 702);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(91, 17);
			this->label2->TabIndex = 34;
			this->label2->Text = L"Messagebox:";
			// 
			// butDiffSave
			// 
			this->butDiffSave->Location = System::Drawing::Point(260, 670);
			this->butDiffSave->Name = L"butDiffSave";
			this->butDiffSave->Size = System::Drawing::Size(205, 23);
			this->butDiffSave->TabIndex = 35;
			this->butDiffSave->Text = L"�nderungen speichern";
			this->butDiffSave->UseVisualStyleBackColor = true;
			this->butDiffSave->Click += gcnew System::EventHandler(this, &DatensatzEingeben::butDiffSave_Click);
			// 
			// butSave
			// 
			this->butSave->Location = System::Drawing::Point(260, 696);
			this->butSave->Name = L"butSave";
			this->butSave->Size = System::Drawing::Size(205, 23);
			this->butSave->TabIndex = 36;
			this->butSave->Text = L"Speichern";
			this->butSave->UseVisualStyleBackColor = true;
			this->butSave->Click += gcnew System::EventHandler(this, &DatensatzEingeben::butSave_Click);
			// 
			// butDelete
			// 
			this->butDelete->Location = System::Drawing::Point(260, 721);
			this->butDelete->Name = L"butDelete";
			this->butDelete->Size = System::Drawing::Size(205, 23);
			this->butDelete->TabIndex = 37;
			this->butDelete->Text = L"Student l�schen";
			this->butDelete->UseVisualStyleBackColor = true;
			this->butDelete->Click += gcnew System::EventHandler(this, &DatensatzEingeben::butDelete_Click);
			// 
			// DatensatzEingeben
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(487, 756);
			this->Controls->Add(this->butDelete);
			this->Controls->Add(this->butSave);
			this->Controls->Add(this->butDiffSave);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->MessageBox);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->groupStud);
			this->Name = L"DatensatzEingeben";
			this->Text = L"DatensatzEingeben";
			this->Shown += gcnew System::EventHandler(this, &DatensatzEingeben::DatensatzEingeben_Shown);
			this->groupStud->ResumeLayout(false);
			this->groupStud->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		
#pragma endregion


public: System::Void tboxStudMatr_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tBoxStudName_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudVorName_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudDatum_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudPhoneHome_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudPhoneMobile_KeyPress				(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudEmail_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudStra�e_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudHausNr_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudPLZ_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxStudOrt_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxPraxName_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxPraxStreet_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxPraxHausNr_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxPraxPLZ_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxPraxOrt_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxBetrName_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxBetrVorName_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxBetrPos_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxBetrPhoneHome_KeyPress					(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxBetrPhoneMobile_KeyPress				(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);
public: System::Void tboxBetrEmail_KeyPress						(System::Object^	sender, System::Windows::Forms::KeyPressEventArgs^  e);



public: System::Void butDiffSave_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void butSave_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void butDelete_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void DatensatzEingeben_Shown(System::Object^  sender, System::EventArgs^  e);
};
}
