//Christoph Ebler 
//MatNr 51411
//16.09.2015
#include "StdAfx.h"
#include <stdio.h>
#include<conio.h>
//Diese fuktion endh�lt das haubtmen�e sie wird direkt aus dem hauptprogramm aufgerufen.
//Ihr werden keine parameter �bergeben
//Sie  giebt den index des ausgew�hlten eintrages an das Hauptprogramm zur�ck!
char mainMenue ()
{

	char eingabe ='0';
	printf("\nWas wollen sie tun ?\n");
		printf("1 = Datensatz Eingeben  \n");
		printf("2 = Datensatz Ausgeben  \n");
		printf("3 = Datensatz Suchen\n");
		printf("4 = Datensatz Bearbeiten\n");
		printf("5 = Datensatz Loeschen\n");
		printf("6 = Datenbasis Speichern\n");
		printf("7 = Datenbasis neu Laden\n");
		
		printf("0 = Beenden \n");
		printf("ihre Wahl ?:");
		scanf("%c",&eingabe);
		printf("\n");
		return(eingabe);
}


//dieses Men�e wird aufgerufen wenn die datei mit den dates�tzen nicht ge�fnet werden konnte.
//Sie  giebt den index des ausgew�hlten eintrages an das Hauptprogramm zur�ck!
int OpenDatFailMenue ()
{
	int eingabe =0;
	printf("\nWas wollen sie tun ?\n");
		printf("1 = Neue Datenbasis anlegen \n"),
		printf("0 = Beenden \n");
		printf("ihre Wahl ?:");
		scanf("%i",&eingabe);
		printf("\n");
		return(eingabe);
}


//Diese fuktion endh�lt das Eingabe Men�  aufgerufen wenn ein datensatz eingegeben werden soll .
//Ihr werden keine parameter �bergeben
//Sie  giebt den index des ausgew�hlten eintrages an die  Aufrufende Funktion  zur�ck!
char EingabeMenue()
{       char eingabe ='0';
	    printf("\nWas wollen sie tun ?\n");
		printf("1 => Speichern\n");
		printf("2 => Einen weiteren studenten eingeben\n");
		printf("0 => Beenden\n ");																																								
		printf("ihre Wahl ?:");
		scanf("%c",&eingabe);
		printf("\n");
		return(eingabe);
}


//Diese fuktion endh�lt das Ausgabe Men� sie wird aufgerufen wenn ein datensatz ausgegeben wurde .
//Ihr werden keine parameter �bergeben
//Sie  giebt den index des ausgew�hlten eintrages an die  Aufrufende Funktion  zur�ck!
char AusgabeMenue()
{       char eingabe ='0';
	    printf("\nWas wollen sie tun ?\n");
		printf("6 => Naechster Datensatz\n");
		printf("4 => Vorheriger Datensatz\n");
		printf("0 => Beenden und zurueck zum Haubtmenue\n");
	    printf("ihre Wahl ?:");
		scanf("%c",&eingabe);
		printf("\n");
		return(eingabe);
}

//Diese fuktion endh�lt das Suchergebnis men� Men� sie wird aufgerufen wenn ein Suchergenbnis ausgegeben wurde .
//Ihr werden keine parameter �bergeben
//Sie  giebt den index des ausgew�hlten eintrages an die  Aufrufende Funktion  zur�ck!
char SuchergMenue(bool bearbeiten,bool loeschen )
{       char eingabe ='0';
	    printf("\nWas wollen sie tun ?\n");
		printf("4 => Vorheriger Datensatz\n");
		printf("6 => Naechster Datensatz\n");
		
		if (bearbeiten==true)
		{
		printf("7 => Datensatz Bearbeiten \n");
		}
		if (loeschen==true)
		{
		printf("8 => Datensatz Loeschen \n");
		}
		printf("0 => Beenden und zurueck zum Haubtmenue\n");
		
	    printf("ihre Wahl ?:");
		scanf("%c",&eingabe);
		printf("\n");
		return(eingabe);
}

//Diese fuktion endh�lt das Suchkriterien  Men� sie wird aufgerufen wenn ein datensatz gesucht werden soll .
//Ihr werden keine parameter �bergeben
//Sie  giebt den index des ausgew�hlten eintrages an die  Aufrufende Funktion  zur�ck!
char SuchKritMenue (char text[20])
{
	char eingabe =' ';
	printf("\n%s\n\n",text);
	printf("\nNach welchem Kriterium wollen sie suchen ?\n\n");
		printf("1 = Nach Matr Nummer  \n");
		printf("2 = Nach Name   \n");
		printf("3 = Nach Vorname \n");
		printf("4 = Nach Praxispartner \n");
		
		printf("0 = Beenden \n");
		printf("ihre Wahl ?:");
		scanf("%c",&eingabe);
		printf("\n");
		return(eingabe);
}
