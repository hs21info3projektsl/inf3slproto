//Christoph Ebler 
//MatNr 51411
//16.09.2015
#include "StdAfx.h"
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
//diese Funktion Leert den Bildschirm 
// sie verwendet nur system.cls solte dieses programm mal auf nem anderen system laufen kann ich hier auch einen anderen befehl einf�gen. 
void myCls()
{
	system("cls");
}

// diese funktion wrid aufgerufen wenn ein falsches Steuerzeichen eingegeben wird.
// sie besteht im grunde nur aus ausgeben 
void strZerror()
{	printf("-------------------------------------------------------------------- \n");
	printf("Sie haben ein fasches Steuerzeichen eingegeben \n");
	printf("Bitte Geben sich ein steuerzeichen aus der Auswahl ein!!! \n");
	printf("Druecken sie einene Belibige taste um in das Menue zurueck zu gelangen \n");
	printf("-------------------------------------------------------------------- \n\n");
	fflush(stdin);
	_getch();
	 myCls();
}

// diese funktion wird aufgerufen wenn die daten erfolgreich gespeichert wurden.
// sie besteht im grunde nur aus ausgeben 
void saveDone()
	
{
	 myCls();
	printf("-------------------------------------------------------------------- \n");
	printf("Das Speichern war erfolgreich \n");
	printf("Druecken sie einene Belibige taste um in das Menue zurueck zu gelangen \n");
	printf("-------------------------------------------------------------------- \n\n");
	fflush(stdin);
	_getch();
	 myCls();
}

//Diese funktion wird aufgerufen wenn kein Platz im Array zur verf�gung steht 
// sie besteht im grunde nur aus ausgeben 
void SpeicherVollError()
	
{
	 myCls();
	printf("-------------------------------------------------------------------- \n");
	printf("Der Speicher fuer die Studenten ist voll. Bitte l�schen sie zun�chst einen Datensatz  \n");
	printf("Druecken sie einene Belibige taste um in das Menue zurueck zu gelangen \n");
	printf("-------------------------------------------------------------------- \n\n");
	fflush(stdin);
	_getch();
	 myCls();
}

//Diese funktion wird aufgerufen wenn kein suchergebnis gefunden wurde
// sie besteht im grunde nur aus ausgeben 
void KeinSuchergebnisError()

{
	 myCls();
	printf("-------------------------------------------------------------------- \n");
	printf("Es wurde kein Datensatz mit dem suchbegriff gefunden \n");
	printf("Druecken sie einene Belibige taste um in das Menue zurueck zu gelangen \n");
	printf("-------------------------------------------------------------------- \n\n");
	fflush(stdin);
	_getch();
	 myCls();
}

//Diese funktion wird aufgerufen wenn der gew�hlte datensatz nicht existiert.
// sie besteht im grunde nur aus ausgeben 
void DSnot_Exsiting_error()
{	printf("-------------------------------------------------------------------- \n");
	printf("Der von ihnen gewaehlte Datensatz Existiert nicht \n");
	printf("Druecken sie einene Belibige taste um in das Menue zurueck zu gelangen \n");
	printf("-------------------------------------------------------------------- \n\n");
	fflush(stdin);
	
}

//Diese funktion wird aufgerufen wenn eingeben beendet wurde ohne das die daten gespeichert wurden 
// hier wird auch noch abgefragt ob die daten gespeichert werden sollen.
// dieser wert wird zur�ckgegebn 
bool No_Save_error()

{	
	bool antwort = false;
	char eingabe =' ';
	do {
		 myCls();
	printf("-------------------------------------------------------------------- \n");
	printf("Der von ihnen Eingegebene Datensatz wurde noch nicht gespeichert\n");
	printf(" ? Moechten sie die daten jetzt Speichern ?*\n");
	printf("N fuer Nein ?<-achtung die �nderungen gehen verloren \n");
	printf("J fuer Ja ? \n");
	printf("-------------------------------------------------------------------- \n");
	printf("ihre Wahl ?:");
	fflush(stdin);
	scanf("%c",&eingabe);
	printf("\n");
	printf("-------------------------------------------------------------------- \n\n");
	fflush(stdin);
	
	switch (eingabe)
	{
		
				case 'J':		
					antwort = true;
					return true ;
					break;
				case 'N':		
					antwort= true;
					return false ;
					break;
				case 'j':		
					antwort = true;
					return true ;
					break;
				case 'n':		
					antwort = true;
					return false ;
					break;
				default :
					strZerror();
					antwort = false;
					break;
	}
	}while(antwort ==false);
	
}