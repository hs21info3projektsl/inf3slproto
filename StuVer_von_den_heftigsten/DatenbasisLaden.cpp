#include "StdAfx.h"
#include "DatenbasisLaden.h"
#include "Form1.h"
#include "fileIO.h"
#include "keyfunc.h"


using namespace StuVer_von_den_heftigsten;
using namespace System;
using namespace System::Runtime::InteropServices;

System::Void StuVer_von_den_heftigsten::DatenbasisLaden::�bernehmen_Click(System::Object^  sender, System::EventArgs^  e){
			Form1^ frm1 = (Form1^)this->Owner; /* CForm1-Objekt*/
			frm1->path_frm1->Text = this->textBox1->Text;
			int erg;
			char filename[] = "";
			bool freeSpace = false;
			if(this->textBox1->Text != "")										//Wenn ein Pfad gew�hlt wurde
			{
				
				char* filename = (char*)(void*)Marshal::StringToHGlobalAnsi(textBox1->Text);
				erg = readFile(filename);											//Datei laden
				if(erg == 1)														//gibt 1, wenn Datei ge�ffnet werden konnte
				{
					freeSpace = SpeicherplatzFrei();								//�berpr�fen, ob die Datenbasis voll ist
					if(freeSpace)
					{
						frm1->textBox1->Text = "Laden OK; Es gibt freie Pl�tze";		//schreibe in die Messagebox in Form1
						frm1->eingabeDB->Enabled = true;
						frm1->augabeDB->Enabled = true;
					}
					else
					{
						frm1->textBox1->Text = "Laden OK, Keine freien Pl�tze";
						frm1->eingabeDB->Enabled = false;
						frm1->augabeDB->Enabled = true;
					};
				} else {															//gibt 0 wenn nicht
					frm1->textBox1->Text = "Datei konnte nicht ge�ffnet werden";//schreibe in die Messagebox in Form1
					frm1->eingabeDB->Enabled = false;
					frm1->augabeDB->Enabled = false;
				}
			};
			Close();
		} 
System::Void StuVer_von_den_heftigsten::DatenbasisLaden::read_file_Click(System::Object^  sender, System::EventArgs^  e) {
			OpenFileDialog^ openFileDialog1 = gcnew OpenFileDialog;		//Erzeuge Objekt openFileDialog
			openFileDialog1->Filter = "txt files (*.txt)|*.txt";		//Konfiguriere Filedialog: Nur .txt Dateien erlaubt
			openFileDialog1->FilterIndex = 2;							//kein plan is kopiert...


			if ( openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK )
			{
				this->textBox1->Text = openFileDialog1->FileName;				//Pfad als String als Eigenschaft von textBox1 setzen
			};


		 }