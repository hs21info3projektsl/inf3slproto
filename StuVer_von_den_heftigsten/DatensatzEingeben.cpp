#include "StdAfx.h"
#include "dataStruckt.h"
#include "DatensatzEingeben.h"
#include <string>
#include "DatensatzAusgeben.h"
#include "keyfunc.h"


using namespace System::Runtime::InteropServices;


System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudMatr_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
	{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
	};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tBoxStudName_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudVorName_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudDatum_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08 && e->KeyChar != 0x2E)//nur Backspace und Zahlen und Punkt erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudPhoneHome_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08 && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudPhoneMobile_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08 && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudEmail_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
	//vorerst alle zeichen erlaubt
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudStra�e_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudHausNr_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudPLZ_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxStudOrt_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxPraxName_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxPraxStreet_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxPraxHausNr_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxPraxPLZ_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxPraxOrt_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxBetrName_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxBetrVorName_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxBetrPos_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08)//nur Backspace und Buchstaben erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxBetrPhoneHome_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08 && e->KeyChar != 0x08)//nur Backspace und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxBetrPhoneMobile_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		if(!Char::IsNumber(e->KeyChar) && e->KeyChar != 0x08 && e->KeyChar != 0x2B)//nur Backspace , + und Zahlen erlaubt
		e->Handled = true;
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::tboxBetrEmail_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
		//vorerst alles erlaubt
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::butSave_Click(System::Object^  sender, System::EventArgs^  e)
{
	extern STUDENT studDB[150];
	if(tboxStudMatr->Text != "" && tBoxStudName->Text != "" && tboxStudVorName->Text != "")
	{
		//Funktion ausf�hren...
		int Nr = find_Free_Space();

		studDB[Nr].RELEVANT				= true;			//wird als wichtig erkannt und in .txt Datei geschrieben
		sprintf(studDB[Nr].MatNr,"%s",Convert::ToString(tboxStudMatr->Text));
		sprintf(studDB[Nr].persStudent.Name,"%s",Convert::ToString(tBoxStudName->Text));
		sprintf(studDB[Nr].persStudent.Vorname,"%s",Convert::ToString(tboxStudVorName->Text));
		sprintf(studDB[Nr].beginnDat,"%s",Convert::ToString(tboxStudDatum->Text));
		sprintf(studDB[Nr].persStudent.festTel,"%s",Convert::ToString(tboxStudPhoneHome->Text));
		sprintf(studDB[Nr].persStudent.mobilTel,"%s",Convert::ToString(tboxStudPhoneMobile->Text));
		sprintf(studDB[Nr].persStudent.eMail,"%s",Convert::ToString(tboxStudEmail->Text));
		sprintf(studDB[Nr].adrStudent.strasse,"%s",Convert::ToString(tboxStudStra�e->Text));
		sprintf(studDB[Nr].adrStudent.nr,"%s",Convert::ToString(tboxStudHausNr->Text));
		sprintf(studDB[Nr].adrStudent.plz,"%s",Convert::ToString(tboxStudPLZ->Text));
		sprintf(studDB[Nr].adrStudent.ort,"%s",Convert::ToString(tboxStudOrt->Text));
		sprintf(studDB[Nr].pxPartner,"%s",Convert::ToString(tboxPraxName->Text));
		sprintf(studDB[Nr].pxAdresse.strasse,"%s",Convert::ToString(tboxPraxStreet->Text));
		sprintf(studDB[Nr].pxAdresse.nr	,"%s",Convert::ToString(tboxPraxHausNr->Text));
		sprintf(studDB[Nr].pxAdresse.plz,"%s",Convert::ToString(tboxPraxPLZ->Text));
		sprintf(studDB[Nr].pxAdresse.ort,"%s",Convert::ToString(tboxPraxOrt->Text));
		sprintf(studDB[Nr].persAnsprech.Name,"%s",Convert::ToString(tboxBetrName->Text));
		sprintf(studDB[Nr].persAnsprech.Vorname,"%s",Convert::ToString(tboxBetrPos->Text));
		sprintf(studDB[Nr].pxPos,"%s",Convert::ToString(tboxStudMatr->Text));
		sprintf(studDB[Nr].persAnsprech.festTel,"%s",Convert::ToString(tboxBetrPhoneHome->Text));
		sprintf(studDB[Nr].persAnsprech.mobilTel,"%s",Convert::ToString(tboxBetrPhoneMobile->Text));
		sprintf(studDB[Nr].persAnsprech.eMail,"%s",Convert::ToString(tboxBetrEmail->Text));


		MessageBox->Text = "Alles in Ordung";
		Close();
	}
	else
	{
		MessageBox->Text = "Bitte alle Pflichtfelder ausf�llen";
	};
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::butDiffSave_Click(System::Object^	sender, System::EventArgs^  e)
{
	this->MessageBox->Text = "DiffSave reagiert";
	extern STUDENT studDB[150];
	if(tboxStudMatr->Text != "" && tBoxStudName->Text != "" && tboxStudVorName->Text != "")
	{
		//Keinen Freien Platz suchen, da dieser als Index �bergeben wird
		extern int gridViewIndex;
		int Nr = gridViewIndex;


		studDB[Nr].RELEVANT				= true;			//wird als wichtig erkannt und in .txt Datei geschrieben
		sprintf(studDB[Nr].MatNr,"%s",Convert::ToString(tboxStudMatr->Text));
		sprintf(studDB[Nr].persStudent.Name,"%s",Convert::ToString(tBoxStudName->Text));
		sprintf(studDB[Nr].persStudent.Vorname,"%s",Convert::ToString(tboxStudVorName->Text));
		sprintf(studDB[Nr].beginnDat,"%s",Convert::ToString(tboxStudDatum->Text));
		sprintf(studDB[Nr].persStudent.festTel,"%s",Convert::ToString(tboxStudPhoneHome->Text));
		sprintf(studDB[Nr].persStudent.mobilTel,"%s",Convert::ToString(tboxStudPhoneMobile->Text));
		sprintf(studDB[Nr].persStudent.eMail,"%s",Convert::ToString(tboxStudEmail->Text));
		sprintf(studDB[Nr].adrStudent.strasse,"%s",Convert::ToString(tboxStudStra�e->Text));
		sprintf(studDB[Nr].adrStudent.nr,"%s",Convert::ToString(tboxStudHausNr->Text));
		sprintf(studDB[Nr].adrStudent.plz,"%s",Convert::ToString(tboxStudPLZ->Text));
		sprintf(studDB[Nr].adrStudent.ort,"%s",Convert::ToString(tboxStudOrt->Text));
		sprintf(studDB[Nr].pxPartner,"%s",Convert::ToString(tboxPraxName->Text));
		sprintf(studDB[Nr].pxAdresse.strasse,"%s",Convert::ToString(tboxPraxStreet->Text));
		sprintf(studDB[Nr].pxAdresse.nr	,"%s",Convert::ToString(tboxPraxHausNr->Text));
		sprintf(studDB[Nr].pxAdresse.plz,"%s",Convert::ToString(tboxPraxPLZ->Text));
		sprintf(studDB[Nr].pxAdresse.ort,"%s",Convert::ToString(tboxPraxOrt->Text));
		sprintf(studDB[Nr].persAnsprech.Name,"%s",Convert::ToString(tboxBetrName->Text));
		sprintf(studDB[Nr].persAnsprech.Vorname,"%s",Convert::ToString(tboxBetrPos->Text));
		sprintf(studDB[Nr].pxPos,"%s",Convert::ToString(tboxStudMatr->Text));
		sprintf(studDB[Nr].persAnsprech.festTel,"%s",Convert::ToString(tboxBetrPhoneHome->Text));
		sprintf(studDB[Nr].persAnsprech.mobilTel,"%s",Convert::ToString(tboxBetrPhoneMobile->Text));
		sprintf(studDB[Nr].persAnsprech.eMail,"%s",Convert::ToString(tboxBetrEmail->Text));


		MessageBox->Text = "Alles in Ordung";
		Close();	//Nach Aktion Muss Fenster geschlossen, werden, da keine erneute aktion ausgef�hrt werden darf, da extern gridViewIndex gel�scht wurde
	}
	else
	{
		MessageBox->Text = "Bitte alle Pflichtfelder ausf�llen";
	};
};
System::Void StuVer_von_den_heftigsten::DatensatzEingeben::butDelete_Click(System::Object^	sender, System::EventArgs^  e)
{
	extern int gridViewIndex;
	int Nr = gridViewIndex;	//Holt den Index des Ausgew�hlten Studenten aus dem Datagridview
	initZeroOne(Nr);		//Ruft Funktion auf, welches alle Eintr�ge in Zelle des ADTs nullterminiert und die Flag f�r l�schen setzt
	MessageBox->Text = "DeleteStud reagiert";
	Close();	//Nach Aktion Muss Fenster geschlossen, werden, da keine erneute aktion ausgef�hrt werden darf, da extern gridViewIndex gel�scht wurde
};

System::Void  StuVer_von_den_heftigsten::DatensatzEingeben::DatensatzEingeben_Shown(System::Object^  sender, System::EventArgs^  e){
	extern int callIndex;
	extern STUDENT studDB[150];
	extern int gridViewIndex;
	int Nr = gridViewIndex;
	if(callIndex == 1){				//Wurde aus Gridview aufgerufen
		tboxStudMatr->Text			=System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].MatNr)); 
		tBoxStudName->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persStudent.Name));
		tboxStudVorName->Text		= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persStudent.Vorname));
		tboxStudDatum->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].beginnDat));
		tboxStudPhoneHome->Text		= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persStudent.festTel));
		tboxStudPhoneMobile->Text	= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persStudent.mobilTel));
		tboxStudEmail->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persStudent.eMail));
		tboxStudStra�e->Text		= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].adrStudent.strasse));
		tboxStudHausNr->Text		= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].adrStudent.nr));
		tboxStudPLZ->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].adrStudent.plz));
		tboxStudOrt->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].adrStudent.ort));

		tboxPraxName->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].pxPartner));
		tboxPraxStreet->Text		= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].pxAdresse.strasse));
		tboxPraxHausNr->Text		= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].pxAdresse.nr));
		tboxPraxPLZ->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].pxAdresse.plz));
		tboxPraxOrt->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].pxAdresse.ort));

		tboxBetrName->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persAnsprech.Name));
		tboxBetrVorName->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persAnsprech.Vorname));
		tboxBetrPos->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].pxPos));
		tboxBetrPhoneHome->Text		= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persAnsprech.festTel));
		tboxBetrPhoneMobile->Text	= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persAnsprech.mobilTel));
		tboxBetrEmail->Text			= System::Runtime::InteropServices::Marshal::PtrToStringAnsi(IntPtr(studDB[Nr].persAnsprech.eMail));
	}
};





