#pragma once



namespace StuVer_von_den_heftigsten {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.

		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::TextBox^  textBox1;
	protected: 


	public: System::Windows::Forms::GroupBox^  groupBox1;
	public: System::Windows::Forms::GroupBox^  groupBox2;
	public: System::Windows::Forms::Button^  searchDB;

	public: System::Windows::Forms::Button^  eingabeDB;
	public: System::Windows::Forms::Button^  augabeDB;
	public: System::Windows::Forms::Button^  saveDB;
	public: System::Windows::Forms::Button^  ladenDB;
	public: System::Windows::Forms::TextBox^  path_frm1;

	public: 




	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->path_frm1 = (gcnew System::Windows::Forms::TextBox());
			this->searchDB = (gcnew System::Windows::Forms::Button());
			this->eingabeDB = (gcnew System::Windows::Forms::Button());
			this->augabeDB = (gcnew System::Windows::Forms::Button());
			this->saveDB = (gcnew System::Windows::Forms::Button());
			this->ladenDB = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(19, 48);
			this->textBox1->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(353, 22);
			this->textBox1->TabIndex = 0;
			this->textBox1->Text = L"Alles in Ordnung";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->path_frm1);
			this->groupBox1->Controls->Add(this->searchDB);
			this->groupBox1->Controls->Add(this->eingabeDB);
			this->groupBox1->Controls->Add(this->augabeDB);
			this->groupBox1->Controls->Add(this->saveDB);
			this->groupBox1->Controls->Add(this->ladenDB);
			this->groupBox1->Font = (gcnew System::Drawing::Font(L"Arial", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->groupBox1->Location = System::Drawing::Point(15, 11);
			this->groupBox1->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBox1->Size = System::Drawing::Size(500, 276);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Was m�chten Sie tun\?";
			// 
			// path_frm1
			// 
			this->path_frm1->Font = (gcnew System::Drawing::Font(L"Arial", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->path_frm1->Location = System::Drawing::Point(23, 75);
			this->path_frm1->Name = L"path_frm1";
			this->path_frm1->ReadOnly = true;
			this->path_frm1->Size = System::Drawing::Size(445, 22);
			this->path_frm1->TabIndex = 11;
			// 
			// searchDB
			// 
			this->searchDB->Enabled = false;
			this->searchDB->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->searchDB->Location = System::Drawing::Point(277, 180);
			this->searchDB->Margin = System::Windows::Forms::Padding(4);
			this->searchDB->Name = L"searchDB";
			this->searchDB->Size = System::Drawing::Size(167, 31);
			this->searchDB->TabIndex = 10;
			this->searchDB->Text = L"Datensatz suchen";
			this->searchDB->UseVisualStyleBackColor = true;
			this->searchDB->Click += gcnew System::EventHandler(this, &Form1::searchDB_Click);
			// 
			// eingabeDB
			// 
			this->eingabeDB->Enabled = false;
			this->eingabeDB->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->eingabeDB->Location = System::Drawing::Point(277, 142);
			this->eingabeDB->Margin = System::Windows::Forms::Padding(4);
			this->eingabeDB->Name = L"eingabeDB";
			this->eingabeDB->Size = System::Drawing::Size(211, 31);
			this->eingabeDB->TabIndex = 8;
			this->eingabeDB->Text = L"Datenbasis eingeben";
			this->eingabeDB->UseVisualStyleBackColor = true;
			this->eingabeDB->Click += gcnew System::EventHandler(this, &Form1::eingabeDB_Click);
			// 
			// augabeDB
			// 
			this->augabeDB->Enabled = false;
			this->augabeDB->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->augabeDB->Location = System::Drawing::Point(25, 142);
			this->augabeDB->Margin = System::Windows::Forms::Padding(4);
			this->augabeDB->Name = L"augabeDB";
			this->augabeDB->Size = System::Drawing::Size(207, 31);
			this->augabeDB->TabIndex = 7;
			this->augabeDB->Text = L"Datensatz ausgeben";
			this->augabeDB->UseVisualStyleBackColor = true;
			this->augabeDB->Click += gcnew System::EventHandler(this, &Form1::augabeDB_Click);
			// 
			// saveDB
			// 
			this->saveDB->Enabled = false;
			this->saveDB->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->saveDB->Location = System::Drawing::Point(277, 104);
			this->saveDB->Margin = System::Windows::Forms::Padding(4);
			this->saveDB->Name = L"saveDB";
			this->saveDB->Size = System::Drawing::Size(167, 31);
			this->saveDB->TabIndex = 6;
			this->saveDB->Text = L"Datenbasis save";
			this->saveDB->UseVisualStyleBackColor = true;
			this->saveDB->Click += gcnew System::EventHandler(this, &Form1::saveDB_Click);
			// 
			// ladenDB
			// 
			this->ladenDB->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->ladenDB->Location = System::Drawing::Point(16, 26);
			this->ladenDB->Margin = System::Windows::Forms::Padding(4);
			this->ladenDB->Name = L"ladenDB";
			this->ladenDB->Size = System::Drawing::Size(167, 31);
			this->ladenDB->TabIndex = 5;
			this->ladenDB->Text = L"Datenbasis laden";
			this->ladenDB->UseVisualStyleBackColor = true;
			this->ladenDB->Click += gcnew System::EventHandler(this, &Form1::ladenDB_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->textBox1);
			this->groupBox2->Location = System::Drawing::Point(15, 292);
			this->groupBox2->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBox2->Size = System::Drawing::Size(500, 100);
			this->groupBox2->TabIndex = 6;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Messagebox:";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(590, 419);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->Name = L"Form1";
			this->Text = L"Studentenverwaltung";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

public: System::Void ladenDB_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void saveDB_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void augabeDB_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void eingabeDB_Click(System::Object^  sender, System::EventArgs^  e);
public: System::Void searchDB_Click(System::Object^  sender, System::EventArgs^  e);


};
}

